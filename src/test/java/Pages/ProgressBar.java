package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProgressBar extends PageObject {
    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testers_hub;

    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a/span")
    public WebElement demo_test_site;

    @FindBy(xpath = "//*[@id=\"menu-item-2832\"]/a/span")
    public WebElement progress_bar_menu;

    @FindBy(xpath = "//button[text() = 'Start Download']")
    public WebElement download_button;

    @FindBy(xpath = "//*[@id=\"post-2671\"]/div[2]/div/div/div[1]/p/iframe")
    public WebElement iframe;

    @FindBy(xpath = "//*[@id=\"dialog\"]/div[1]")
    public WebElement file_download_complete;

    public ProgressBar(WebDriver driver) {
        super( driver );
    }

    public void navigateToProgressBarPage() {
        Actions actions = new Actions( driver );
        actions.moveToElement( testers_hub )
                .moveToElement( demo_test_site ).perform();
        progress_bar_menu.click();
    }

    public void clickToDownloadButton() {
        navigateToProgressBarPage();
        driver.switchTo().frame( iframe );
        download_button.click();
    }

    public void waitForDownloading() {
        wait.until( ExpectedConditions.textToBePresentInElement( file_download_complete, "Complete!" ) );
    }
}
