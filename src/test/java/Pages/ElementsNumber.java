package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

// All locators and methods are identified here

public class ElementsNumber extends PageObject {
    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testers_hub;

    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a/span")
    public WebElement demo_test_site;

    @FindBy(xpath = "//li[text()=\"First Step\"]/following-sibling::li")
    public List<WebElement> first_column;

    @FindBy(xpath = "//li[text()=\"Second Step\"]/following-sibling::li")
    public List<WebElement> second_column;

    @FindBy(xpath = "//li[text()=\"Third Step\"]/following-sibling::li")
    public List<WebElement> third_column;


    public ElementsNumber(WebDriver driver) {
        super( driver );
    }


    public void navigateToDemoTestingSite() {
        Actions actions = new Actions( driver );
        actions.moveToElement( testers_hub ).click( demo_test_site ).perform();
    }

}
