package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTime extends PageObject {


    @FindBy(xpath = "//*[@id=\"menu-item-2822\"]/a")
    public WebElement testers_hub;

    @FindBy(xpath = "//*[@id=\"menu-item-2823\"]/a/span")
    public WebElement demo_test_site;

    @FindBy(xpath = "//*[@id=\"menu-item-2827\"]/a/span")
    public WebElement date_picker_site;

    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;

    @FindBy(xpath = "//*[@id=\"datepicker\"]")
    public WebElement date_input_box;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[2]/td[4]")
    public WebElement current_day;


    public DateTime(WebDriver driver) {
        super( driver );
    }


    public void navigateToDatePickerPage() {
        Actions actions = new Actions( driver );
        actions.moveToElement( testers_hub ).moveToElement( demo_test_site ).perform();
        date_picker_site.click();
    }

    public void pickTheDate() {
        driver.switchTo().frame( iframe );
        date_input_box.click();
        current_day.click();
    }


    public boolean verifyValidDate() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "MM/dd/yyyy" );
        String dateValue = date_input_box.getAttribute( "value" );
        LocalDate localDate = LocalDate.parse( dateValue, formatter );
        LocalDate today = LocalDate.now();
        return localDate.equals( today );

    }


}
