package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PageObject {

    WebDriver driver;
    WebDriverWait wait;


    public PageObject(WebDriver driver) {
        wait = new WebDriverWait( driver, 8 );
        this.driver = driver;
        PageFactory.initElements( driver, this );
    }

}