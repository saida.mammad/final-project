package TestSuit;

import Pages.DateTime;
import Pages.ElementsNumber;
import Pages.Utils;
import Pages.ProgressBar;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class TestCases {
    private WebDriver driver;
    private WebDriverWait wait;
    Utils utils;


    @BeforeEach
    public void setPage() {
        driver = new ChromeDriver();
        wait = new WebDriverWait( driver, 30 );
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS );
        driver.get( utils.BASE_URL );

    }

    @AfterEach
    public void cleanUp() {
        driver.manage().deleteAllCookies();
        driver.quit();
    }

    @Test
    // Check the number of Items in the columns
    public void checkColumnsItems() {
        int expectedColumnNumber = 6;
        ElementsNumber elementsNumber = new ElementsNumber( driver );
        elementsNumber.navigateToDemoTestingSite();
        Assert.assertEquals( elementsNumber.first_column.size(), expectedColumnNumber );
        Assert.assertEquals( elementsNumber.second_column.size(), expectedColumnNumber );
        Assert.assertEquals( elementsNumber.third_column.size(), expectedColumnNumber );

    }

    @Test
    // Check the Date Format
    public void checkDateFormat() {
        DateTime dateTime = new DateTime( driver );
        dateTime.navigateToDatePickerPage();
        dateTime.pickTheDate();
        Assert.assertTrue( dateTime.verifyValidDate() );
    }

    @Test
    // Check the Progress Bar Downloading
    public void checkProgressBar() {
        ProgressBar progressBar = new ProgressBar( driver );
        progressBar.clickToDownloadButton();
        progressBar.waitForDownloading();
        Assert.assertTrue( progressBar.file_download_complete.isDisplayed() );

    }
}
